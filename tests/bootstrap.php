<?php
/**
 * Load WordPress test environment
 */

ini_set( 'display_errors','on' );
error_reporting( E_ALL );

$tests_dir    = dirname( __FILE__ );
$plugin_dir   = dirname( $tests_dir );
$wp_tests_dir = getenv( 'WP_TESTS_DIR' ) ? getenv( 'WP_TESTS_DIR' ) : $plugin_dir . '/tmp/wordpress-tests-lib';

$GLOBALS['wp_tests_options'] = array(
	'active_plugins' => array( "jc-woocommerce-advanced-attributes/jc-woocommerce-advanced-attributes.php" ),
);

// load the WP testing environment
if( $wp_tests_dir . '/includes/bootstrap.php' ){
	require_once( $wp_tests_dir . '/includes/bootstrap.php' );
}else{
	exit( "Couldn't find path to wordpress-tests/bootstrap.php\n" );
}
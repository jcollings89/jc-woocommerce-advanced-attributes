module.exports = function(grunt) {

    var currDate = new Date();

    var day = currDate.getUTCDate();
    var month = (currDate.getUTCMonth() + 1);
    if(day < 10){
        day = '0' + day;
    }
    if(month < 10){
        month = '0' + month;
    }

    var formattedDate = day + '/' + month + '/'+currDate.getUTCFullYear()

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            main: {
                files: [
                    {expand: true, src: ['libs/**'], dest: 'build/tmp/'},
                    {expand: true, src: ['languages/**'], dest: 'build/tmp/'},
                    {expand: true, src: ['assets/**'], dest: 'build/tmp/'},
                    {expand: true, src: ['views/**'], dest: 'build/tmp/'},
                    {expand: false, src: ['README.md'], dest: 'build/tmp/README.md'},
                    {expand: false, src: ['README.html'], dest: 'build/tmp/README.html'},
                    {expand: false, src: ['<%= pkg.name %>.php'], dest: 'build/tmp/<%= pkg.name %>.php'}
                ]
            }
        },
        clean: {
            build: ["build/tmp/*", "!build/.svn/*"],
        },
        compress: {
            main: {
                options: {
                    archive: 'build/<%= pkg.name %>-v<%= pkg.version %>.zip'
                },
                files: [
                    {cwd: 'build/tmp/', src: ['**'], dest: '<%= pkg.name %>/'},
                ]
            }
        },
        markdown: {
            all: {
                files: [
                    {
                        expand: true,
                        src: 'README.md',
                        dest: './',
                        ext: '.html'
                    }
                ],
                options: {
                    template: 'markdown-template.jst',
                }
            }
        },
        'string-replace': {
            dist:{
                files:[{
                  expand: true,
                  cwd: './',
                  src: ['<%= pkg.name %>.php', 'README.md']
                  // dest: '<%= pkg.name %>.php'
                }],
                options:{
                    replacements: [
                        {
                            pattern: /Version: ([0-9a-zA-Z\-\.]+)/m,
                            replacement: 'Version: <%= pkg.version %>'
                        },
                        {
                            pattern: /\$version = '([0-9a-zA-Z\-\.]+)';/m,
                            replacement: '$version = \'<%= pkg.version %>\';'
                        },
                        {
                            pattern: /^Updated: ([0-9\/]+)/m,
                            replacement: 'Updated: '+formattedDate
                        }
                    ]
                }
            }
        },
        makepot: {
            target: {
                options: {
                    type: 'wp-plugin',
                    domainPath: '/languages',
                    mainFile: 'jc-woocommerce-advanced-attributes.php',
                    exclude: ['.idea/*', 'node_modules/*', 'build/*', 'tests/*']
                }
            }
        }
    });

    // grunt modules
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-markdown');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-wp-i18n');

    // Default task(s).
    grunt.registerTask('default', ['makepot', 'string-replace', 'markdown', 'clean:build', 'copy', 'compress' /*, 'clean:build'*/]);
    grunt.registerTask('version', ['string-replace']);
    grunt.registerTask('doc', ['markdown']);

};